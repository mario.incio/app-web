import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { MaterialModule } from 'src/app/material.module';
import { RouterModule } from "@angular/router";

import { LiquidacionComponent } from '../modules/liquidacion/liquidacion.component';
import { CajaComponent } from '../modules/caja/caja.component';

@NgModule({
    declarations: [
        HeaderComponent, SidebarComponent, MainComponent, FooterComponent, LiquidacionComponent, CajaComponent
    ],
    imports:[
        MaterialModule,
        RouterModule
    ],
    exports:[
        MainComponent,
    ]
})

export class MainModule {}