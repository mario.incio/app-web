import { Component, OnInit } from '@angular/core';
const EXAMPLE_DATA: any[] = [
  {
    id: 33,
    idliquidacion: 38,
    ruc: 7777777777,
    tipodocumento: '1',
    numerodocumento: 77777777,
    fecha: '2020-11-02',
    moneda: 1,
    concepto: 'ninguno',
    contabilidad: 213123,
    centrocosto: 1,
    monto: 2000,
    igv: 1,
    created_at: '2020-04-01 21:53:59',
    updated_at: '2020-04-01 21:53:59',
  }
];
@Component({
  selector: 'app-liquidacion',
  templateUrl: './liquidacion.component.html',
  styleUrls: ['./liquidacion.component.css']
})

export class LiquidacionComponent implements OnInit {

  constructor() { }
  public data: any = EXAMPLE_DATA;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'id',
    'idliquidacion',
    'ruc',
    'tipodocumento',
    'numerodocumento',
    'fecha',
    'moneda',
    'concepto',
    'contabilidad',
    'centrocosto',
    'monto',
    'igv'
    ];
  ngOnInit(): void {
  }

  nuevoMetodo() : void{
  


  }

}
