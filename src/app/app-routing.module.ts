import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashbordComponent } from "./components/modules/dashbord/dashbord.component";
import { MuestreoComponent } from './components/modules/muestreo/muestreo.component';
import { MainComponent } from './components/main/main/main.component';
import { LiquidacionComponent } from './components/modules/liquidacion/liquidacion.component';
import { CajaComponent } from "./components/modules/caja/caja.component";

const routes: Routes = [{
  path: '',
  component: MainComponent,
  children:[{
    path: '',
  component: DashbordComponent
  },{
    path: 'dashbord',
    component: DashbordComponent
  },{
    path: 'caja',
    component: CajaComponent
  },{
  path: 'muestreo',
  component: MuestreoComponent
  },{
    path: 'liquidacion',
    component: LiquidacionComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
